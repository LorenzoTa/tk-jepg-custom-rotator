
![Alt text](src-images/screenshot01.jpg?raw=true "screenshot")

<h1><a class='u' href='#___top' title='click to go to top of document'
name="DESCRIPTION"
>DESCRIPTION</a></h1>

<p>Jpg custom rotation,
resize and crop.
The program loads a list of jpg images and let you to apply one or more of the following modification:</p>

<ul>
<li>A custom rotation setting two points (P1 and P2) on a preview image using mouse buttons.
The center of the line P1-P2 will be used as center of the rotation.
The rotation will be always applied to make P2 on the right side of the resulting image.</li>

<li>A custom resizing of the image specifying a fixed size for the line P1-P2 in the destination image.
The resulting image will be streatched or reduced accordingly.</li>

<li>A custom cropping of the image specifying a target resolution for the destination image.
The image will be cropped around the center.</li>
</ul>

<h1><a class='u' href='#___top' title='click to go to top of document'
name="SYNOPSIS"
>SYNOPSIS</a></h1>

<p>Command line options:</p>

<pre>   --forcelinesize N
       makes the line to be exactly N pixel in the destination photo
       stretching or enlarging the file accordingly. This switch forces 
       &#39;enable resizing&#39; in the control panel to a true value.
   
   --cropwidth N
       fix the width of the destiantion image. This option forces 
       &#39;enable cropping&#39; in the control panel to a true value. Both 
       cropwidth and cropheight must be specified
   
   --cropheight N
       fix the height of the destiantion image
       
   --devdebug
       enable more verbose output and the creation of debug images at 
       each step of the processing</pre>

<h1><a class='u' href='#___top' title='click to go to top of document'
name="KEY_BINDINGS"
>KEY BINDINGS</a></h1>

<p>Preview Window:</p>

<pre>    Button-1    (left click)    set point1
    Button-3    (right click)   set point2
    space                       modify the photo and loads the next one
    Button-2    (middle click)  modify the photo and loads the next one
    p                           reload the previous photo
    n                           skip the current photo and load the next one</pre>

<h1><a class='u' href='#___top' title='click to go to top of document'
name="HOW_IT_WORKS"
>HOW IT WORKS</a></h1>

<p>The program consists of a control panel and a preview area. The informative output is written to the console used to launch the program. In the control panel you build the list of images to process, add or remove image transformations, control the name used to save images and control the debug level and can access this documentation.</p>

<h3><a class='u' href='#___top' title='click to go to top of document'
name="Choose_P1_and_P2"
>Choose P1 and P2</a></h3>

<p>A preview image is created from the original image using the specified <code>ratio</code> and you can left click with the mouse on it to set P1. You can click several times to set P1 to a new position.</p>

<p>Right clicking on the preview image sets P2, but only if P1 is already set. At this point a line is drawn in red on the preview image with an arrow on P2 and a little yellow circle to show the center of the line.</p>

<p>Once points are set, hitting <code>Button-2</code> (the midlle mouse button) or <code>spacebar</code> all enabled transformations are applied to the original image and the result is saved with the name composed using the logic the user choosen in the control panel.</p>

<h3><a class='u' href='#___top' title='click to go to top of document'
name="Rotation"
>Rotation</a></h3>

<p>The rotation of the image is made firstly cropping the original image at the center between P1 and P2. The original size is used and a black background is used. This temporary images is then rotated to make the P1-P2 line to be horizontal and P2 on the right side.</p>

<h3><a class='u' href='#___top' title='click to go to top of document'
name="Resize"
>Resize</a></h3>

<p>If enabled and a fixed size is specified for the P1-P2 line then the image is reduced or enlarged accordingly.</p>

<h3><a class='u' href='#___top' title='click to go to top of document'
name="Cropping"
>Cropping</a></h3>

<p>The crop is made around the center between P1 and P2 using width and height specified in the control panel.</p>

<h1><a class='u' href='#___top' title='click to go to top of document'
name="DEBUG"
>DEBUG</a></h1>

<p>Beside normal verbose output activated by <code>--debug</code> command line switch or by the dedicated check in the GUI, there is also a <code>--devdebug</code> switch that add more verbose output but also writes an image at each stage of the prcessing with points and lines in the relevant positions. These images will have fixed names and will be ovewritten when a new jpeg is processed. While <code>--devdebug</code> is active the preview window has another binding: <code>CRTL + d</code> which dumps on the screen the relevant part of the object.</p>

<h1><a class='u' href='#___top' title='click to go to top of document'
name="AUTHOR"
>AUTHOR</a></h1>

<p>This program was done by LorenzoTa and the main repository can be found at <a href="https://gitlab.com/LorenzoTa/tk-jepg-custom-rotator" class="podlinkurl"
>gitlab</a></p>

<p>The main support site for this software is <a href="https://perlmonks.org" class="podlinkurl"
>perlmonks.org</a> where i can be reached as <a href="https://www.perlmonks.org/index.pl?node_id=174111" class="podlinkurl"
>Discipulus</a></p>

<h1><a class='u' href='#___top' title='click to go to top of document'
name="LICENSE"
>LICENSE</a></h1>

<p>This software is povided as is and it is licensed under the same terms of Perl itself.</p>

<!-- end doc -->

</body></html>
